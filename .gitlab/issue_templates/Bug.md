Summary

(Da el resumen del bug)

Stepts to reproduce

(Indica los pasos para reproducir el bug)

What is the current behavior ? 

(cual es el comportamiento actual)

What is the expected correct behavior ? 

(cual es el comportamiento esperado)

Relevant logs and/or screenshots

(Por favor añade cualquier log o imagen relevante para identificar el error, utiliza (```) para escribir el codigo y el output de la consola, si fuese dificil de leer de otra manera)

/label ~Bug